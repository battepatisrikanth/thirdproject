// "111.139.161.143"

function ipToInteger(ipAddress){
    validValues=[1,2,3,4,5,6,7,8,9,0];
    for(let ipIndex=0;ipIndex<ipAddress.length;ipIndex++){
        if (!((ipAddress[ipIndex] in validValues) || ipAddress[ipIndex] =='.')){
            return [];
        }
    }
    
    let octet=ipAddress.split('.')
    
    if(octet.length!=4){
        return [];
    }

    for (let index=0;index<octet.length;index++){
       if(Number(octet[index])>255 || Number(octet[index])<0){
        return [];
       }
    }

    
    let resultArray=[];
    let bufferArray=ipAddress.split('.');
    for(let index=0;index<bufferArray.length;index++){
    
        resultArray.push(Number(bufferArray[index]));
    }
    return resultArray;

}
module.exports=ipToInteger;