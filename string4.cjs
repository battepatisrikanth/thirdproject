function fullNameTitleCase(nameObject){
    if(nameObject== undefined || nameObject==null || typeof(nameObject)!= "object"){
        return "";
    }
    let resultString=String('');
    let bufferString=String('');
    for(let key in nameObject){
        bufferString=String(nameObject[key]);
        bufferString=bufferString.toLowerCase();
        resultString=resultString+bufferString.charAt(0).toUpperCase()+bufferString.slice(1)+' ';
    }
    

    return resultString;

}

module.exports=fullNameTitleCase;