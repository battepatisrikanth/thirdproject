function arrayToString(arrayOfStrings){
    if(arrayOfStrings== undefined || arrayOfStrings==null || Array.isArray(arrayOfStrings)==false){
        return "";
    }
    for(let index=0;index<arrayOfStrings.length;index++){
        if(typeof(arrayOfStrings[index])!="string"){
            return '';
        }
    }
    resultString='';
    for(let index=0; index<arrayOfStrings.length; index++){
        resultString+= arrayOfStrings[index] +' ';
    }
    return resultString;
}

module.exports=arrayToString;