
function stringToOnlyNumber(inputArray){
    if(inputArray== undefined || inputArray==null || Array.isArray(inputArray)==false){
        return "";
    }

    validValues=['0','1','2','3','4','5','6','7','8','9'];
    resultArray=[];
    for(let index=0;index<inputArray.length;index++){
        let resultString='';
        let readString=inputArray[index];
        // console.log(typeof(validValues[10]));
        // console.log(validValues[10]==='-')
        for (let itemIndex=0;itemIndex<readString.length;itemIndex++){
            // if(readString.charAt(itemIndex) =='$' || readString.charAt(itemIndex) == ','){
                if((readString.charAt(itemIndex) in validValues)|| readString.charAt(itemIndex)=='.'||readString.charAt(itemIndex)=='-'){
                    // console.log(readString.charAt(itemIndex)== validValues[10]);
                    // console.log(readString.charAt(itemIndex)); 
                    resultString=resultString+readString[itemIndex];
                    // console.log(resultString);
                
            }
            else{
                // console.log(readString.charAt(itemIndex),typeof(readString.charAt(itemIndex)));
                continue;
                // console.log(readString[itemIndex]);
                //["$100.45", "$1,002.22", "-$123","$845"]
            }
    
        }
        resultArray.push(Number(resultString));
    
        

        }
   
    return resultArray;
}
module.exports=stringToOnlyNumber;
        


