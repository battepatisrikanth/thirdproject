function monthFromDate(date){
    let months=['January','Febrauary','March','April','May','June','July','August','September','October','November','December'];
    let bufferArray=date.split('/');
    validValues=['0','1','2','3','4','5','6','7','8','9']
    if(date== undefined || date==null || typeof(date)!="string"){
        return "";
    }
    for(let index=0;index<date.length;index++){
        if(!((date[index] in validValues) || date[index]=='/')){
            return '';
        }
    }
    if(Number(bufferArray[0])< 0 || Number(bufferArray[0])>30  || Number(bufferArray[1])< 0 ||Number(bufferArray[1])>12 || Number(bufferArray[2])< 0 ){
        return '';
    }

    
    return months[Number(bufferArray[1]-1)];
}

module.exports=monthFromDate;